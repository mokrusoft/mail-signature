<?php
function serverName($remove) {
  $path = "";
  if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
    $path = "https://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."template".DIRECTORY_SEPARATOR;
  } else {
    $path = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."template".DIRECTORY_SEPARATOR;
  }
  return str_replace($remove, "", $path);
}
$name = "YOUR NAME";
$phone = "000 000 000 000";
$phone2 = "000 111 222 333";
$email = "yourmail@yourdomain.com";
$department = "Your Department";
$path = serverName("preview.php");

$inputFile = fopen("template/index.html", "r") or die("No file found");
$outputFile = fread($inputFile, filesize("template/index.html"));
fclose($inputFile);
$outputFile = str_replace("{name}", $name, $outputFile);
$outputFile = str_replace("{phone}", $phone, $outputFile);
$outputFile = str_replace("{phone2}", $phone2, $outputFile);
$outputFile = str_replace("{department}", $department, $outputFile);
$outputFile = str_replace("{email}", $email, $outputFile);
$outputFile = str_replace('src="', 'src="'.$path, $outputFile);

echo $outputFile;