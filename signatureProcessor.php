<?php require_once "App/init.php"; if(!$session->isLogged()) {exit;}
  function serverName() {
    $path = "";
    if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
      $path = "https://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."template".DIRECTORY_SEPARATOR;
    } else {
      $path = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."template".DIRECTORY_SEPARATOR;
    }
    return str_replace("signatureProcessor.php", "", $path);
  }





	function bodytext($name = "", $phone = "", $phone2 = "", $email = "", $department = "") {
		$inputFile = fopen("template/index.html", "r") or die("No index.html file found");
		$outputFile = fread($inputFile, filesize("template/index.html"));
		fclose($inputFile);
    $outputFile = str_replace("{name}", $name, $outputFile);
    $outputFile = str_replace("{phone}", $phone, $outputFile);
    $outputFile = str_replace("{phone2}", $phone2, $outputFile);
    $outputFile = str_replace("{department}", $department, $outputFile);
    $outputFile = str_replace("{email}", $email, $outputFile);
    $outputFile = str_replace('src="', 'src="'.serverName(), $outputFile);
		return $outputFile;
	}
	function createFileName($text) {
		return strtoupper(str_replace(" ", "-", $text))."-".date("dmYHis");
	}
	function createSignatureFile($name, $content = "") {
		$signatureFile = "signature/".$name.".htm";
		
		if($fileHandle = fopen($signatureFile, "w")) {
			fwrite($fileHandle, $content);
			fclose($fileHandle);
		}
		
		
	}
	if (isset($_POST["createsignature"])) {
		// Data provided by the form
		$name = isset($_POST["name"])?$_POST["name"]:"";
		$phone = isset($_POST["phone"])?$_POST["phone"]:"";
		$phone2 = isset($_POST["phone2"])?$_POST["phone2"]:"";
		$dept = isset($_POST["dept"])?$_POST["dept"]:"";
		$email = isset($_POST["email"])?$_POST["email"]:"";
		$method = $_POST["method"];
		$mailreceiver = $_POST["mailreceiver"];
		// Creates name with concatenated first and last names + date
		$filenamedate = createFileName($name);
		// Creates .htm file in "Firma" directory
		createSignatureFile($filenamedate, bodytext($name, $phone, $phone2, $email, $dept));
		// Creates ZIP File Name, same as previous .htm file inside "Firma" directory
		$zipFile = "signature/".$filenamedate.".zip";
		$zip = new ZipArchive();
		// Check if we can create ZIP file
		if ($zip->open($zipFile, ZipArchive::CREATE)!==TRUE) {
			echo(json_encode(["status"=>"error"]));
			exit();
		}
		$zip->addFile("signature/".$filenamedate.".htm");
		$zip->addFile("signature/Install-Signature.bat");
		$zip->addFile("signature/Install-Instructions.pdf");
		$zip->close();
		// Delete .htm file after zipping
		$deleteFile = "signature/".$filenamedate.".htm";
		unlink($deleteFile);
		// var for nameinfo for deletion after downloading
		$deleteAfter = $filenamedate.".zip";
		// include here all file writes
		
		if($method == "download") {
      echo json_encode(["status"=> "download", "link"=>$deleteAfter]);
    } else {
      echo json_encode(["status"=> "send", "receiver"=>$mailreceiver, "link"=>$deleteAfter]);
    }
	}
	
	if(isset($_POST["deleteZip"])) {
		$fileToDelete = "signature/".$_POST["delfile"];
		if(unlink($fileToDelete)) {
			echo json_encode(["status"=>"done"]);
		} else {
			echo json_encode(["status"=>"error"]);
		}
		
	}
  
  if(isset($_POST["sendMail"])) {
    $fileToDelete = "signature/".$_POST["delfile"];
    $userSend = $_POST["destiny"];
    if(MMailer::sendEmail($userSend, translate("your_new_signature"), translate("signature_attached"), translate("signature_attached"), $fileToDelete)) {
      if(unlink($fileToDelete)) {
        echo json_encode(["status"=>"done"]);
      } else {
        echo json_encode(["status"=>"error"]);
      }
    } else {
      echo json_encode(["status"=>"error"]);
    }
    
    
  }