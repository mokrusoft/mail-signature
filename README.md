<img src="docs/images/logo.png" alt="Logo" width="120"/>

#  Mail HTML Signatures Deploy System

> *An easy to use HTML Signature deployment system for distributing HTML signatures to your company employees and co-workers, accessing via web interface, setting customizable fields and automatic instalation on Microsoft Outlook*


---

### Requirements:

- Server with PHP 7+ 

---


### Included Frameworks and Libraries:

- [jQuery](https://jquery.com)
- [Bootstrap](https://getbootstrap.com)
- [PHPMailer](https://github.com/PHPMailer/PHPMailer)

---
## Installation:
### Prepare your signature to deploy

Create your signature in `index.html` file and put it inside `template` folder with your images
```
/template 📂
    index.html
    logo.png
    anotherImage.jpg
```
**IMPORTANT:** Use *"double quotes"* for any `src=""` tags, the system will bind the absolut path to the images using the domain name, http, https, etc.

*Customizable fields are:*
- `name` (Full Name)
- `phone`
- `phone2` (For Additional Phone numbers)
- `department`
- `email` 

Inside your code user these filed inside curly brackets {} like this: 
```html
<tr>
  <td>{name}</td>
</tr>
<tr>
  <td>{phone}</td>
</tr>
```
Instide the original template folder you'll find an example code

### Prepare the deploy to server:

Inside /App folder personalize `configs.php` file, updating PHP constants:
- `DOMAIN` *domain name allowed to access the platform*
- `EMAIL` *Email address for sending notifications*
- `USERNAME` *Username for that mailbox*
- `PASSWORD` *Password for that mailbox*
- `SENDER_NAME` *Sender Name for (From) Field when you receive the notification*
- `PARAMS` (PHP Array) *Remove those custom field you don't need in your signature*
- `LANG` *Language preference with two accepted parameters: `es` for Spanish and `en` for English, to display the user interface in selected language*

### Upload to your server:

Upload all the contents to your server at any folder like `signature` to access it later: https://yourdomain.com/ `signature`. Remove `/docs` folder before uploading. NOTE: You can preview your signature installed by going to `/preview` or `/preview.php`


### Install on Outlook Client:

Once downloaded the signature, UNZIP it anywhere. Close Outlook. *You should have at least one signature previously created in Outlook.* Double click `Install-Signature.bat`, the command will copy the singature file to `sigantures` folder on your machine, then go to Outlook and select your new signature. For Mac user the instructions will be added later or you can find lots of tutorials online