<?php require_once "App/init.php";
function pinMail($pin) {
  $output = '
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
<head>
  <title>'.translate("access_pin").'</title>
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500" rel="stylesheet" type="text/css" />
  <style type="text/css">* {
      font-family: "Poppins", sans-serif !important;
    }
    a,
    a:hover,
    a:visited,
    a:active,
    a:focus {
      text-decoration: none !important;
      color: grey !important;
    }
    a[href] { color: #000; text-decoration: none !important;}
  </style>
</head>
<body style="word-wrap: break-word; -webkit-nbsp-mode: space;">
<center>
<table width="100%" style="width: 80%;" cellpadding="4" cellspacing="1">
  <tbody>
  <tr>
    <td>
      <p style="text-align:center; font-family: sans-serif;
       font-weight: bold; font-size: 60px;">'.$pin.'</p>
    </td>
  </tr>
  </tbody>
</table>
</center>
</body>
</html>
  ';
  return $output;
}
  
  function generatePin() {
    global $session;
    $pin = rand(1111, 9999);
    $session->setPin($pin);
    return $pin;
  }
  function verifyEmail($email) {
    preg_match("/@".DOMAIN."$/", $email, $matches);
    if($matches) {
      MMailer::sendEmail($email, translate("access_pin"), pinMail(generatePin()));
      return true;
      
    } else {
      return false;
    }
  }

  

// Verifications

if(isset($_POST["sendVerify"])) {
  
  if(verifyEmail($_POST["user"])) {
    echo(json_encode(["status"=>"done"]));
  } else {
    echo(json_encode(["status"=>"error", "message"=>translate("not_company")]));
  }
}

if(isset($_POST["verifyPin"])) {
  $pin = $_POST["pin"];
  if($session->verifyPin($pin)) {
    echo(json_encode(["status"=>"done"]));
  } else {
    echo(json_encode(["status"=>"error"]));
  }
}