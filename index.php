<?php require_once "App/init.php"; if(!$session->isLogged()) {header("Location: login");};checkParams();?>
<!doctype html>
<html lang="en">
<head>
  <meta name="robots" content="noindex">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<title><?=translate("customize_sign")?></title>
</head>
<body>
<div class="container pt-3">
	<div class="row">
		<div class="col-12">
			<h1 class="text-center"><?=translate("customize_sign")?></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<form action="" method="post" class="signForm">
        <?php if (isset($_POST["name"])): ?>
				<div>
					<label class="mb-0" for="name">
						<small><?=translate("fullname")?>:</small></label>
					<input class="form-control" id="name" type="text" placeholder="<?=translate("fullname")?>" name="name">
				</div>
        <?php endif;
        if (isset($_POST["phone"])):?>
				<div>
					<label class="mb-0" for="phone">
						<small><?=translate("phone")?>:</small></label>
					<input class="form-control" type="tel" placeholder="<?=translate("phone")?>" id="phone" name="phone">
				</div>
        <?php endif;
        if (isset($_POST["phone2"])):?>
        <div>
          <label class="mb-0" for="phone2">
            <small><?=translate("phone2")?>:</small></label>
          <input class="form-control" type="tel" placeholder="<?=translate("phone2")?>" id="phone2" name="phone2">
        </div>
        <?php endif;
        if (isset($_POST["email"])):?>
        <div>
          <label class="mb-0" for="email">
            <small><?=translate("email")?>:</small></label>
          <input class="form-control" type="email" placeholder="Email" id="email" name="email">
        </div>
        <?php endif;
        if (isset($_POST["department"])):?>
				<div>
					<label class="mb-0" for="dept">
						<small><?=translate("department")?>:</small></label>
					<input class="form-control" type="text" placeholder="<?=translate("department")?>" id="dept" name="dept">
				</div>
        <?php endif; ?>
        
        <div class="custom-control custom-switch py-2">
          <input type="checkbox" class="custom-control-input" id="formail">
          <label class="custom-control-label" for="formail"><?=translate("send_by")?>
          </label>
        </div>
        
        
        
        
        
        <div class="mailer" style="display: none;">
          <label for="eml" class="d-none">
            <small><?=translate("co_email")?>:</small></label>
          <input class="form-control" type="text" placeholder="<?=translate("co_email")?>" id="eml" name="eml">
        </div>
        <div class="mt-3">
          <input id="create" class="btn btn-outline-info float-right" type="submit" value="<?=translate("create")?>" name="createsignature">
        </div>
			</form>
			
		</div>
	</div>
	<div class="row">
		<div class="col-12 text-center py-3">
			<div class="messenger">
        <a style="display: none" class="download btn btn-outline-info" href="#" download><?=translate("download")?></a>
      </div>
		</div>
    <div class="col-12 text-center py-3">
      <div class="sender">
        <button style="display: none;" class="send btn btn-outline-info"><?=translate("send_by")?></button>
      </div>
    </div>
	</div>
  <div class="row">
    <div class="col-12 text-center py-3">
      <button id="reset" class="btn btn-outline-info" style="display: none;">
        <?=translate("go_back")?>
      </button>
    </div>
  </div>
</div>
<script>
  let file;
  let maildestiny;
  
	$("#create").click(function (e){
    e.preventDefault();
	  let method;
    if($("#formail").is(":checked")) {
      method = "mail";
      
    } else {
      method = "download";
    }
    let userReceiver = $("#eml").val();
		$("#message").fadeIn();
		let form = $(".signForm");
		$.post("signatureProcessor.php", {
			createsignature: "true",
      
      name: $("#name").val(),
      phone: $("#phone").val(),
      phone2: $("#phone2").val(),
      email: $("#email").val(),
      dept: $("#dept").val(),
      
     method: method,
     mailreceiver: userReceiver
		}, function (data) {
			switch(data.status){
        case "download":
			  file = data.link;
				form.slideUp();
				$(".download").prop("href", "signature/"+data.link).fadeIn("slow");
				break;
        case "send":
          file = data.link;
          maildestiny = data.receiver;
          form.slideUp();
          $(".send").fadeIn();
          // add action
          break;
        default:
          $(".messenger").text("Algo no ha ido bien, intentalo de nuveo más tarde!");
          break;
			}
		}, "json");
		
	});
	$("#formail").change(function (e) {
    e.preventDefault();
    $(".mailer").fadeToggle();
  });
  $(".download").click(function () {
	  
    setTimeout(function () {
      $.post("signatureProcessor.php", {
        deleteZip: "true",
        delfile: file
      }, function (data) {
        let messenger = $(".messenger");
        switch (data.status) {
          case "done":
            $(".download").fadeOut();
            messenger.html("<p class='text-center'><?=translate("download_complete")?></p>");
            $("#reset").fadeIn();
            break;
          case "error":
            messenger.html("<p class='text-center'><?=translate("errors")?></p>");
            break;
        }
      }, "json");
    }, 400);
  });

  $(".send").click(function (e) {
    $(this).attr("disabled", true);
    e.preventDefault();
    $.post("signatureProcessor.php", {
      sendMail: "true",
      delfile: file,
      destiny: maildestiny
    }, function (data) {
      let messenger = $(".messenger");
      switch (data.status) {
        case "done":
          $(".send").fadeOut();
          messenger.html("<p class='text-center'><?=translate("sent_by_mail")?></p>");
          $("#reset").fadeIn();
          break;
        case "error":
          messenger.html("<p class='text-center'><?=translate("errors")?></p>");
          break;
      }
    }, "json");
    
  });
  $("#reset").click(function () {
    location.reload();
  });
	
</script>
</body>
</html>
