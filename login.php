<?php require_once("App/init.php");?>
<!doctype html>
<html lang="es">
<head>
  <meta name="robots" content="noindex">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <title>Login</title>
</head>
<body>
<div class="container">
  <div class="row py-4 vertical align-items-center">
    
    
    <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3" id="start">
      <h2 class="text-center"><?=translate("welcome")?></h2>
      <p class="text-muted text-center"><?=translate("use_your_mail")?>:</p>
      <div class="input-group mb-3">
        <input id="user" type="email" class="form-control" placeholder="Email:">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button" id="request"><?=translate("request")?></button>
        </div>
      </div>
      <p class="errors">
      
      </p>
    </div>
    
    <div class="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3" id="pins" style="display: none;">
      <p class="text-center text-muted"><?=translate("use_pin")?></p>
      <div class="pins">
        <div class="input-group text-center">
          <input id="firstdigint" type="tel" class="form-control mx-1 text-center rounded nums" autofocus maxlength="1">
          <input id="seconddigit" type="tel" class="form-control mx-1 text-center rounded nums" maxlength="1">
          <input id="thirddigit" type="tel" class="form-control mx-1 text-center rounded nums" maxlength="1">
          <input id="lastdigit" type="tel" class="form-control mx-1 text-center rounded nums" maxlength="1">
        </div>
      </div>
      <p class="pt-3 errors-pins">

      </p>
    </div>
    
  </div>
  <script>
    $("#request").click(function (e) {
      e.preventDefault();
      $(this).attr("disabled", "true");
      let user = $("#user").val();
      let errors = $(".errors");
      $.post("forms", {
        sendVerify: "true",
        user: user
      }, function (data) {
        switch (data.status) {
          case "done":
            errors.hide();
            $("#start").hide();
            $("#pins").fadeIn("slow");
            break;
          case "error":
            errors.text(data.message);
            break;
        }
      }, "json");
      
    });
    $("#user").change(function (e) {
      e.preventDefault();
      $("#request").removeAttr("disabled");
    });
    let errorCount = 0;
    $("input.nums").keyup(function (e) {
      if(e.keyCode === 8) {
        $(this).prev("input").focus();
      } else {
        $(this).attr("type", "password");
        $(this).next("input").focus();
      }
    });
    $("#lastdigit").keyup(function (e) {
      
      e.preventDefault();
      let first = $("#firstdigint").val();
      let second = $("#seconddigit").val();
      let third = $("#thirddigit").val();
      let last = $("#lastdigit").val();
      $.post("forms", {
        verifyPin: "true",
        pin: first+second+third+last
      }, function (data) {
        switch (data.status) {
          case "done":
            $(".pins").fadeOut("slow");
            setTimeout(function () {
              window.location.href = "index";
            }, 400);
            break;
          case "error":
            errorCount++;
            $(".errors-pins").text("<?=translate("pin_error")?>");
            $("input").val("");
            $("#firstdigint").focus();
            if(errorCount === 3) {
              window.location.href = "index";
            }
            
            break;
        }
      
      }, "json");
    });
  </script>
  
</div>
</body>
</html>