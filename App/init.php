<?php
include "configs.php";
require_once "Session.php";
require_once "MMailer.php";
// Function to get available fields
function checkParams() {
    foreach (PARAMS as $key => $value) {
      switch ($value) {
        case "name":
          $_POST["name"] = "name";
          break;
        case "phone":
          $_POST["phone"] = "phone";
          break;
        case "phone2":
          $_POST["phone2"] = "phone2";
          break;
        case "department":
          $_POST["department"] = "department";
          break;
        case "email":
          $_POST["email"] = "email";
          break;
      }
    }
  }
function translate($input) {
  $input = strtolower($input);
$translations = [
"welcome"=>[
    "Welcome",
    "Bienvenid@"
],

"use_your_mail"=>[
    "Use you corporate email to access",
    "Introduce tu Correo Corporativo para acceder"
],

"request"=>[
    "Request",
    "Solicitar"
],

"use_pin"=>[
    "Use the PIN code we sent you to your inbox","Introduce el PIN que has recibido por correo electrónico"
],

"access_pin"=>[
    "Access PIN",
    "Pin de Acceso"
],

"not_company"=>[
    "Your email address is not from this company",
    "Tu correo electrónico no es de esta empresa"
],

"pin_error"=>[
    "Wrong PIN",
    "PIN Erróneo"
],

"customize_sign"=>[
    "Customize Your Signature",
  "Configura tu Firma"
],

"fullname"=>[
    "Full Name",
  "Nombre Completo"
],

"phone"=>[
    "Phone",
  "Teléfono"
],

"phone2"=>[
    "Another Phone",
  "Otro Teléfono"
],

"email"=>[
    "Email",
  "Correo Electrónico"
],

"department"=>[
    "Department",
  "Departamento"
],

"send_by"=>[
    "Send by Email",
  "Enviar por Email"
],

"co_email"=>[
    "Co-worker's email",
  "Email destinatario"
],

"create"=>[
    "Create",
  "Crear"
],

"download"=>[
    "Download",
  "Descargar"
],

"go_back"=>[
    "Go Back",
  "Volver"
],

"errors"=>[
    "Something went wrong, try it again later",
  "Algo ha ido mal, inténtalo de nuevo más tarde"
],

"download_complete"=>[
    "You've just downloaded your new signature, unzip the folder and follow the instructions inside",
  "Acabas de descargar tu nueva firma, descomprime el archivo y sigue las instrucciones en el interior"
],

"sent_by_mail"=>[
    "We've just sent the signature by mail",
  "Acabamos de enviar la firma por correo electrónico"
],

"your_new_signature"=>[
    "Your New Mail Signature",
  "Tu Nueva Firma de Correo Electrónico"
],

"signature_attached"=>[
    "Here is your new signature, unzip the file attached and follow the instructions",
  "Aquí tienes tu nueva firma, descomprime el archivo adjunto y sigue las instrucciones en el interior (en Inglés)"
]


];
  $result = "";
  switch (LANG){
    case "es":
      $result = $translations[$input][1];
      break;
    case "en":
      $result = $translations[$input][0];
      break;
  }
  return $result;
}
  