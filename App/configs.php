<?php
  // Settings
  // Language Settings: "es" for spanish, "en" for english
  const LANG = "en";
  // Authorized Domain to access the platform
  const DOMAIN = "example.com";
  
  // Email Settings
  // Email address
  const EMAIL = "no-replay@example.com";
  // SMTP Username
  const USERNAME = "no-replay@example.com";
  // Password
  const PASSWORD = "Secret123";
  // Sender name (from:)
  const SENDER_NAME = "My Notification";
  
  
  // Configuration Parameters, specify which values you will need:
  // possible options: name, phone, phone2, department, email.
  // If you remove some of them, those field will dissapear from configuration form
  const PARAMS = ["name", "phone", "phone2", "email", "department"];
  
