<?php
  class Session {
    // Properties:
    private $loggedIn = false;
    public $uid;
    public $pin;
    // Methods:
    // !CONSTRUCT METHOD:
    function __construct() {
      session_start();
      ob_start();
      $this->checkLogin();
    }
    // Check Login Method
    private function checkLogin() {
      if(isset($_SESSION["uid"])) {
        $this->uid = $_SESSION["uid"];
        $this->loggedIn = true;
      } else {
        unset($this->uid);
        $this->loggedIn = false;
      }
    }
    // Logged In verification
    public function isLogged() {
      return $this->loggedIn;
    }
    // Login Users
    public function login($pin) {
      $this->uid = $_SESSION["uid"] = $pin;
      $this->loggedIn = true;
    }
    // Logout function
    public function logout() {
      unset($_SESSION["uid"]);
      unset($this->uid);
      $this->loggedIn = false;
    }
    
    public function setPin($pin) {
      $this->pin = $_SESSION["pin"] = $pin;
    }
    
    public function verifyPin($typedPin) {
      if($typedPin == $_SESSION["pin"]) {
        $this->login($typedPin);
        return true;
      } else {
        $this->loggedIn = false;
        return false;
      }
    }
    
  }
  $session = new Session();







































