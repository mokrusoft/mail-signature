<?php
  
  // Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
  require $_SERVER["DOCUMENT_ROOT"]."/PHPMailer/src/PHPMailer.php";
  require $_SERVER["DOCUMENT_ROOT"]."/PHPMailer/src/SMTP.php";
  require $_SERVER["DOCUMENT_ROOT"]."/PHPMailer/src/Exception.php";
  

  class MMailer
  {
    public static function sendEmail($to, $subject, $body, $altBody = "", $attachment = "") {
      $mail = new PHPMailer(true);
      try {
        //Server settings
        $mail->CharSet = "utf-8";
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = DOMAIN;  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = USERNAME;                     // SMTP username
        $mail->Password   = PASSWORD;                               // SMTP password
//        $mail->SMTPSecure = ENCRYPTION;                                  // Enable TLS encryption, `ssl` also accepted
//        $mail->Port       = PORT;                                    // TCP port to connect to
      
        //Recipients
        $mail->setFrom(EMAIL, SENDER_NAME);
        $mail->addAddress($to);     // Add a recipient
//    $mail->addAddress('ellen@example.com');               // Name is optional
//    $mail->addReplyTo('info@example.com', 'Information');
//    $mail->addCC('cc@example.com');
//    $mail->addBCC('bcc@example.com');®
      
        // Attachments
        if($attachment != "") {
          $mail->addAttachment($attachment);
        }
//            // Add attachments
//    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
      
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = $altBody;
      
        $mail->send();
        return true;
      } catch (Exception $e) {
        return false;
      }
    }
  }